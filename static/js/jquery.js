$(document).ready(function() {
    $('.accordion').addClass('dark-theme')
	$('#theme').click(function() {
		$('body').toggleClass('light-theme');
        $('body').toggleClass('dark-theme');
        $('.accordion').toggleClass('light-theme');
        $('.accordion').toggleClass('dark-theme');
    });

    var accord = document.getElementsByClassName('accordion');
    var panel = document.getElementsByClassName('panel');
    for(let i = 0; i < accord.length; i++) {
        accord[i].addEventListener("click", function() {
            panel[i].classList.toggle("active");
    })};
});