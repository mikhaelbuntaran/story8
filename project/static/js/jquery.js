$(document).ready(function() {
    $('.accordion').addClass('dark-theme')
	$('#theme').click(function() {
		$('body').toggleClass('light-theme');
        $('body').toggleClass('dark-theme');
        $('.accordion').toggleClass('light-theme');
        $('.accordion').toggleClass('dark-theme');
        
	});
});