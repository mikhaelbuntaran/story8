from django.shortcuts import render

# Create your views here.
def index(request):
	html = 'landingpage.html'
	return render(request, html)