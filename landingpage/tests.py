from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class LandingpageTest(TestCase):
	def landingpage_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_landingpage_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_using_donation_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'landingpage.html')